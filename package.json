{
  "name": "gitlab-workflow",
  "displayName": "GitLab Workflow",
  "description": "GitLab VSCode integration",
  "version": "3.26.0",
  "publisher": "GitLab",
  "license": "MIT",
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/gitlab-org/gitlab-vscode-extension"
  },
  "engines": {
    "vscode": "^1.52.0"
  },
  "categories": [
    "Other"
  ],
  "keywords": [
    "git",
    "gitlab",
    "merge request",
    "pipeline",
    "ci cd"
  ],
  "capabilities": {
    "virtualWorkspaces": {
      "supported": false,
      "description": "GitLab Workflow doesn't support remote GitLab workspaces yet, please see https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/411 for more information."
    }
  },
  "activationEvents": [
    "*"
  ],
  "bugs": {
    "url": "https://gitlab.com/gitlab-org/gitlab-vscode-extension/issues",
    "email": "incoming+gitlab-org-gitlab-vscode-extension-5261717-issue-@incoming.gitlab.com"
  },
  "galleryBanner": {
    "color": "#805DE7",
    "theme": "dark"
  },
  "main": "out/src/extension",
  "icon": "src/assets/logo.png",
  "contributes": {
    "commands": [
      {
        "command": "gl.setToken",
        "title": "GitLab: Set GitLab Personal Access Token"
      },
      {
        "command": "gl.removeToken",
        "title": "GitLab: Remove your GitLab Personal Access Token"
      },
      {
        "command": "gl.showIssuesAssignedToMe",
        "title": "GitLab: Show issues assigned to me"
      },
      {
        "command": "gl.showMergeRequestsAssignedToMe",
        "title": "GitLab: Show merge requests assigned to me"
      },
      {
        "command": "gl.openActiveFile",
        "title": "GitLab: Open active file on GitLab"
      },
      {
        "command": "gl.copyLinkToActiveFile",
        "title": "GitLab: Copy link to active file on GitLab"
      },
      {
        "command": "gl.openCurrentMergeRequest",
        "title": "GitLab: Open merge request for current branch"
      },
      {
        "command": "gl.openCreateNewIssue",
        "title": "GitLab: Create new issue on current project"
      },
      {
        "command": "gl.openCreateNewMR",
        "title": "GitLab: Create new merge request on current project"
      },
      {
        "command": "gl.openProjectPage",
        "title": "GitLab: Open current project on GitLab"
      },
      {
        "command": "gl.createSnippetPatch",
        "title": "GitLab: Create snippet patch"
      },
      {
        "command": "gl.applySnippetPatch",
        "title": "GitLab: Apply snippet patch"
      },
      {
        "command": "gl.openCurrentPipeline",
        "title": "GitLab: Open current pipeline on GitLab"
      },
      {
        "command": "gl.pipelineActions",
        "title": "GitLab: Pipeline actions - View, Create, Retry or Cancel"
      },
      {
        "command": "gl.issueSearch",
        "title": "GitLab: Search project issues (Supports filters)"
      },
      {
        "command": "gl.mergeRequestSearch",
        "title": "GitLab: Search project merge requests (Supports filters)"
      },
      {
        "command": "gl.projectAdvancedSearch",
        "title": "GitLab: Project Advanced Search (Issues, MR's, commits, comments...)"
      },
      {
        "command": "gl.compareCurrentBranch",
        "title": "GitLab: Compare current branch with master"
      },
      {
        "command": "gl.createSnippet",
        "title": "GitLab: Create snippet"
      },
      {
        "command": "gl.insertSnippet",
        "title": "GitLab: Insert snippet"
      },
      {
        "command": "gl.validateCIConfig",
        "title": "GitLab: Validate GitLab CI config"
      },
      {
        "command": "gl.showOutput",
        "title": "GitLab: Show extension logs"
      },
      {
        "command": "gl.refreshSidebar",
        "title": "GitLab: Refresh sidebar",
        "icon": {
          "light": "src/assets/images/light/refresh.svg",
          "dark": "src/assets/images/dark/refresh.svg"
        }
      },
      {
        "command": "gl.resolveThread",
        "title": "Resolve thread",
        "category": "GitLab",
        "icon": "$(pass)"
      },
      {
        "command": "gl.unresolveThread",
        "title": "Unresolve thread",
        "category": "GitLab",
        "icon": "$(pass-filled)"
      },
      {
        "command": "gl.deleteComment",
        "title": "Delete comment",
        "category": "GitLab",
        "icon": "$(trash)"
      },
      {
        "command": "gl.startEditingComment",
        "title": "Edit Comment",
        "category": "GitLab",
        "icon": "$(edit)"
      },
      {
        "command": "gl.cancelEditingComment",
        "title": "Cancel",
        "category": "GitLab"
      },
      {
        "command": "gl.submitCommentEdit",
        "title": "Save comment",
        "category": "GitLab"
      },
      {
        "command": "gl.createComment",
        "title": "Add comment now",
        "category": "GitLab"
      },
      {
        "command": "gl.checkoutMrBranch",
        "title": "Checkout MR branch"
      },
      {
        "command": "gl.cloneWiki",
        "title": "Clone Wiki",
        "category": "GitLab"
      }
    ],
    "menus": {
      "commandPalette": [
        {
          "command": "gl.resolveThread",
          "when": "false"
        },
        {
          "command": "gl.unresolveThread",
          "when": "false"
        },
        {
          "command": "gl.deleteComment",
          "when": "false"
        },
        {
          "command": "gl.startEditingComment",
          "when": "false"
        },
        {
          "command": "gl.cancelEditingComment",
          "when": "false"
        },
        {
          "command": "gl.submitCommentEdit",
          "when": "false"
        },
        {
          "command": "gl.createComment",
          "when": "false"
        },
        {
          "command": "gl.checkoutMrBranch",
          "when": "false"
        },
        {
          "command": "gl.showIssuesAssignedToMe",
          "when": "gitlab:validState"
        },
        {
          "command": "gl.showMergeRequestsAssignedToMe",
          "when": "gitlab:validState"
        },
        {
          "command": "gl.openActiveFile",
          "when": "gitlab:validState && editorIsOpen"
        },
        {
          "command": "gl.copyLinkToActiveFile",
          "when": "gitlab:validState && editorIsOpen"
        },
        {
          "command": "gl.openCurrentMergeRequest",
          "when": "gitlab:validState"
        },
        {
          "command": "gl.openCreateNewIssue",
          "when": "gitlab:validState"
        },
        {
          "command": "gl.openCreateNewMR",
          "when": "gitlab:validState"
        },
        {
          "command": "gl.openProjectPage",
          "when": "gitlab:validState"
        },
        {
          "command": "gl.openCurrentPipeline",
          "when": "gitlab:validState"
        },
        {
          "command": "gl.pipelineActions",
          "when": "gitlab:validState"
        },
        {
          "command": "gl.issueSearch",
          "when": "gitlab:validState"
        },
        {
          "command": "gl.mergeRequestSearch",
          "when": "gitlab:validState"
        },
        {
          "command": "gl.projectAdvancedSearch",
          "when": "gitlab:validState"
        },
        {
          "command": "gl.compareCurrentBranch",
          "when": "gitlab:validState"
        },
        {
          "command": "gl.createSnippet",
          "when": "gitlab:validState && editorIsOpen"
        },
        {
          "command": "gl.insertSnippet",
          "when": "gitlab:validState && editorIsOpen"
        },
        {
          "command": "gl.validateCIConfig",
          "when": "gitlab:validState && editorIsOpen"
        },
        {
          "command": "gl.refreshSidebar",
          "when": "gitlab:validState"
        },
        {
          "command": "gl.cloneWiki",
          "when": "!gitlab:noToken"
        },
        {
          "command": "gl.createSnippetPatch",
          "when": "gitlab:validState"
        },
        {
          "command": "gl.applySnippetPatch",
          "when": "gitlab:validState"
        }
      ],
      "view/title": [
        {
          "command": "gl.refreshSidebar",
          "when": "view =~ /(currentBranchInfo|issuesAndMrs)/",
          "group": "navigation"
        }
      ],
      "view/item/context": [
        {
          "command": "gl.checkoutMrBranch",
          "when": "view =~ /issuesAndMrs/ && viewItem == mr-item-from-same-project"
        }
      ],
      "comments/comment/title": [
        {
          "command": "gl.startEditingComment",
          "group": "inline@1",
          "when": "commentController =~ /^gitlab-mr-/ && comment =~ /canAdmin/"
        },
        {
          "command": "gl.deleteComment",
          "group": "inline@2",
          "when": "commentController =~ /^gitlab-mr-/ && comment =~ /canAdmin/"
        }
      ],
      "comments/comment/context": [
        {
          "command": "gl.submitCommentEdit",
          "group": "inline@1",
          "when": "commentController =~ /^gitlab-mr-/"
        },
        {
          "command": "gl.cancelEditingComment",
          "group": "inline@2",
          "when": "commentController =~ /^gitlab-mr-/"
        }
      ],
      "comments/commentThread/title": [
        {
          "command": "gl.resolveThread",
          "group": "inline@1",
          "when": "commentController =~ /^gitlab-mr-/ && commentThread == unresolved"
        },
        {
          "command": "gl.unresolveThread",
          "group": "inline@2",
          "when": "commentController =~ /^gitlab-mr-/ && commentThread == resolved"
        }
      ],
      "comments/commentThread/context": [
        {
          "command": "gl.createComment",
          "group": "inline",
          "when": "commentController =~ /^gitlab-mr-/"
        }
      ]
    },
    "viewsContainers": {
      "activitybar": [
        {
          "id": "gitlab-workflow",
          "title": "GitLab Workflow",
          "icon": "src/assets/images/light/gitlab-logo.svg"
        }
      ]
    },
    "views": {
      "gitlab-workflow": [
        {
          "id": "issuesAndMrs",
          "name": "Issues and Merge Requests"
        },
        {
          "id": "currentBranchInfo",
          "name": "For current branch",
          "when": "gitlab:validState"
        }
      ]
    },
    "viewsWelcome": [
      {
        "view": "issuesAndMrs",
        "contents": "Welcome to the GitLab Workflow extension!\nThis extension needs an access token with the 'api' and 'read_user' scopes.\n- GitLab.com users: [create a token on GitLab.com](https://gitlab.com/-/profile/personal_access_tokens?name=GitLab+VS+Code+Extension&scopes=api,read_user).\n- Users on self-managed instances: in GitLab, click your avatar in the top right corner and select 'Preferences.' In the left sidebar, select 'Access Tokens,' then select 'Add a personal access token.'\nAfter you create a token, select 'Set Personal Access Token':\n[Set Personal Access Token](command:gl.setToken)\nTo learn more, read [the setup information](https://gitlab.com/gitlab-org/gitlab-vscode-extension#setup) for this extension.",
        "when": "gitlab:noToken"
      },
      {
        "view": "issuesAndMrs",
        "contents": "No Git repository available. To learn how to fix this, check the Source Control tab.\n[Open Source Control](command:workbench.view.scm)",
        "when": "gitlab:noRepository"
      }
    ],
    "configuration": {
      "title": "GitLab Workflow (GitLab VSCode Extension)",
      "properties": {
        "gitlab.instanceUrl": {
          "type": [
            "string",
            "null"
          ],
          "default": null,
          "description": "Your GitLab instance URL (default is https://gitlab.com)"
        },
        "gitlab.showStatusBarLinks": {
          "type": "boolean",
          "default": true,
          "description": "Whether to display all GitLab related link in the status bar (Requires restart of VSCode)"
        },
        "gitlab.showIssueLinkOnStatusBar": {
          "type": "boolean",
          "default": true,
          "description": "Whether to display the GitLab issue link in the status bar"
        },
        "gitlab.showMrStatusOnStatusBar": {
          "type": "boolean",
          "default": true,
          "description": "Whether to display the GitLab Merge Request status in the status bar"
        },
        "gitlab.ca": {
          "type": "string",
          "default": null,
          "description": "Custom CA file to use (example: /etc/ssl/certs/ca-certificates.crt)"
        },
        "gitlab.cert": {
          "type": "string",
          "default": null,
          "description": "Custom Certificate file to use (example: /etc/ssl/certs/certificate.crt)"
        },
        "gitlab.certKey": {
          "type": "string",
          "default": null,
          "description": "Custom Certificate Key file to use (example: /etc/ssl/certs/certificateKey.key)"
        },
        "gitlab.ignoreCertificateErrors": {
          "type": "boolean",
          "default": false,
          "description": "Ignore TLS/SSL certificate errors when calling the GitLab API"
        },
        "gitlab.remoteName": {
          "type": "string",
          "default": null,
          "description": "Name of the git remote to use in order to locate the Gitlab project"
        },
        "gitlab.pipelineGitRemoteName": {
          "type": "string",
          "default": null,
          "description": "Name of the git remote to use in order to locate the Gitlab project for your pipeline. Keep empty for default"
        },
        "gitlab.showPipelineUpdateNotifications": {
          "type": "boolean",
          "default": false,
          "description": "Show notification in VSCode when pipeline status changed"
        },
        "gitlab.showProjectMergeRequests": {
          "type": "boolean",
          "default": true,
          "description": "Enable the \"All Project Merge Requests\" sidebar pane"
        },
        "gitlab.customQueries": {
          "type": "array",
          "minItems": 1,
          "items": {
            "type": "object",
            "title": "Custom GitLab Query",
            "required": [
              "name"
            ],
            "properties": {
              "name": {
                "type": "string",
                "description": "The label to show in the GitLab panel"
              },
              "maxResults": {
                "type": "number",
                "description": "The maximum number of results to show",
                "default": 20,
                "maximum": 100,
                "minimum": 1
              },
              "orderBy": {
                "type": "string",
                "description": "Return issues ordered by the selected value. It is not applicable for vulnerabilities",
                "enum": [
                  "created_at",
                  "updated_at",
                  "priority",
                  "due_date",
                  "relative_position",
                  "label_priority",
                  "milestone_due",
                  "popularity",
                  "weight"
                ],
                "default": "created_at"
              },
              "sort": {
                "type": "string",
                "description": "Return issues sorted in ascending or descending order. It is not applicable for vulnerabilities",
                "enum": [
                  "asc",
                  "desc"
                ],
                "default": " desc"
              },
              "scope": {
                "type": "string",
                "description": "Return Gitlab items for the given scope. It is not applicable for epics. \"assigned_to_me\" and \"created_by_me\" are not applicable for vulnerabilities. \"dismissed\" is not applicable for issues and merge requests",
                "enum": [
                  "assigned_to_me",
                  "created_by_me",
                  "dismissed",
                  "all"
                ],
                "default": "all"
              },
              "type": {
                "type": "string",
                "description": "The type of GitLab items to return. If snippets is selected, none of the other filter will work. Epics will work only on GitLab ultimate/gold.",
                "enum": [
                  "issues",
                  "merge_requests",
                  "epics",
                  "snippets",
                  "vulnerabilities"
                ],
                "default": "merge_requests"
              },
              "noItemText": {
                "type": "string",
                "description": "The text to show if the query returns no items",
                "default": "No items found."
              },
              "state": {
                "type": "string",
                "description": "Return \"all\" issues or just those that are \"opened\" or \"closed\". It is not applicable for vulnerabilities",
                "enum": [
                  "all",
                  "opened",
                  "closed"
                ],
                "default": "opened"
              },
              "labels": {
                "type": "array",
                "description": "Array of label names, Gitlab item must have all labels to be returned. \"None\" lists all GitLab items with no labels. \"Any\" lists all GitLab issues with at least one label. Predefined names are case-insensitive. It is not applicable for vulnerabilities",
                "items": {
                  "type": "string"
                }
              },
              "milestone": {
                "type": "string",
                "description": "The milestone title. \"None\" lists all GitLab items with no milestone. \"Any\" lists all GitLab items that have an assigned milestone. It is not applicable for epics and vulnerabilities"
              },
              "author": {
                "type": "string",
                "description": "Return GitLab items created by the given username. It is not applicable for vulnerabilities"
              },
              "assignee": {
                "type": "string",
                "description": "Returns GitLab items assigned to the given username. \"None\" returns unassigned GitLab items. \"Any\" returns GitLab items with an assignee. It is not applicable for epics and vulnerabilities"
              },
              "search": {
                "type": "string",
                "description": "Search GitLab items against their title and description. It is not applicable for vulnerabilities"
              },
              "searchIn": {
                "type": "string",
                "description": "Modify the scope of the search attribute. It is not applicable for epics and vulnerabilities",
                "enum": [
                  "all",
                  "title",
                  "description"
                ],
                "default": "all"
              },
              "createdAfter": {
                "type": "string",
                "format": "date",
                "description": "Return GitLab items created after the given date. ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z. It is not applicable for vulnerabilities"
              },
              "createdBefore": {
                "type": "string",
                "format": "date",
                "description": "Return GitLab items created before the given date. ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z. It is not applicable for vulnerabilities"
              },
              "updatedAfter": {
                "type": "string",
                "format": "date",
                "description": "Return GitLab items updated after the given date. ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z. It is not applicable for vulnerabilities"
              },
              "updatedBefore": {
                "type": "string",
                "format": "date",
                "description": "Return GitLab items updated before the given date. ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z. It is not applicable for vulnerabilities"
              },
              "wip": {
                "type": "string",
                "enum": [
                  "yes",
                  "no"
                ],
                "description": "Filter merge requests against their wip status. \"yes\" to return only WIP merge requests, \"no\" to return non WIP merge requests. Works only with merge requests",
                "default": "no"
              },
              "confidential": {
                "type": "boolean",
                "description": "Filter confidential or public issues. Works only with issues",
                "default": false
              },
              "excludeLabels": {
                "type": "array",
                "description": "Array of label names, Gitlab item must not have to be returned. Predefined names are case-insensitive. Works only with issues",
                "items": {
                  "type": "string"
                }
              },
              "excludeMilestone": {
                "type": "string",
                "description": "The milestone title to exclude. Works only with issues"
              },
              "excludeAuthor": {
                "type": "string",
                "description": "Return GitLab items not created by the given username. Works only with issues"
              },
              "excludeAssignee": {
                "type": "string",
                "description": "Returns GitLab items not assigned to the given username. Works only with issues"
              },
              "excludeSearch": {
                "type": "string",
                "description": "Search GitLab items that doesn't have the search key in their title or description. Works only with issues"
              },
              "excludeSearchIn": {
                "type": "string",
                "description": "Modify the scope of the excludeSearch attribute. Works only with issues",
                "enum": [
                  "all",
                  "title",
                  "description"
                ],
                "default": "all"
              },
              "reportTypes": {
                "type": "array",
                "description": "Returns vulnerabilities belonging to specified report types. Works only with vulnerabilities",
                "items": {
                  "type": "string",
                  "enum": [
                    "sast",
                    "dast",
                    "dependency_scanning",
                    "container_scanning"
                  ]
                }
              },
              "severityLevels": {
                "type": "array",
                "description": "Returns vulnerabilities belonging to specified severity levels. Defaults to all. Works only with vulnerabilities",
                "items": {
                  "type": "string",
                  "enum": [
                    "undefined",
                    "info",
                    "unknown",
                    "low",
                    "medium",
                    "high",
                    "critical"
                  ]
                }
              },
              "confidenceLevels": {
                "type": "array",
                "description": "Returns vulnerabilities belonging to specified confidence levels. Defaults to all. Works only with vulnerabilities",
                "items": {
                  "type": "string",
                  "enum": [
                    "undefined",
                    "ignore",
                    "unknown",
                    "experimental",
                    "low",
                    "medium",
                    "high",
                    "confirmed"
                  ]
                }
              },
              "pipelineId": {
                "type": "number | string",
                "description": "Returns vulnerabilities belonging to specified pipeline. \"branch\" returns vulnerabilities belonging to latest pipeline of the current branch. Works only with vulnerabilities"
              },
              "reviewer": {
                "type": "string",
                "description": "Returns GitLab Merge Requests assigned for review to the given username. When set to \"<current_user>\", the extension uses the current user's username."
              }
            }
          },
          "default": [
            {
              "name": "Issues assigned to me",
              "type": "issues",
              "scope": "assigned_to_me",
              "state": "opened",
              "noItemText": "There is no issue assigned to you."
            },
            {
              "name": "Issues created by me",
              "type": "issues",
              "scope": "created_by_me",
              "state": "opened",
              "noItemText": "There is no issue created by you."
            },
            {
              "name": "Merge requests assigned to me",
              "type": "merge_requests",
              "scope": "assigned_to_me",
              "state": "opened",
              "noItemText": "There is no MR assigned to you."
            },
            {
              "name": "Merge requests I'm reviewing",
              "type": "merge_requests",
              "reviewer": "<current_user>",
              "state": "opened",
              "noItemText": "There is no MR for you to review."
            },
            {
              "name": "Merge requests created by me",
              "type": "merge_requests",
              "scope": "created_by_me",
              "state": "opened",
              "noItemText": "There is no MR created by you."
            },
            {
              "name": "All project merge requests",
              "type": "merge_requests",
              "scope": "all",
              "state": "opened",
              "noItemText": "The project has no merge requests"
            }
          ],
          "description": "Custom views in the GitLab panel"
        }
      }
    }
  },
  "extensionDependencies": [
    "vscode.git"
  ],
  "scripts": {
    "vscode:prepublish": "npm run compile",
    "postinstall": "cd src/webview && npm install",
    "compile": "tsc -p ./",
    "watch": "tsc -watch -p ./",
    "test-unit": "jest",
    "test-integration": "npm run compile && node ./out/test/runTest.js",
    "create-test-workspace": "npm run compile && node ./scripts/create_workspace_for_test_debugging.js",
    "test": "npm run test-unit && npm run test-integration",
    "lint": "eslint --ext .js --ext .ts . && prettier --check '**/*.{js,ts,vue,json}' && cd src/webview && npm run lint",
    "autofix": "eslint --fix . && prettier --write '**/*.{js,ts,vue,json}' && cd src/webview && npm run autofix",
    "publish": "vsce publish",
    "webview": "cd src/webview && npm run watch",
    "version": "conventional-changelog -p angular -i CHANGELOG.md -s && git add CHANGELOG.md",
    "update-ci-variables": "node ./scripts/update_ci_variables.js"
  },
  "devDependencies": {
    "@types/jest": "^26.0.20",
    "@types/node": "^13.13.45",
    "@types/request-promise": "^4.1.47",
    "@types/semver": "^7.3.6",
    "@types/sinon": "^9.0.11",
    "@types/temp": "^0.8.34",
    "@types/vscode": "^1.52.0",
    "@typescript-eslint/eslint-plugin": "^4.22.0",
    "@typescript-eslint/parser": "^4.22.0",
    "conventional-changelog-cli": "^2.1.1",
    "eslint": "^7.25.0",
    "eslint-config-airbnb-base": "^14.2.1",
    "eslint-config-prettier": "^6.15.0",
    "eslint-plugin-import": "^2.22.1",
    "jest": "^26.6.3",
    "jest-junit": "^12.0.0",
    "mocha": "^7.0.1",
    "mocha-junit-reporter": "^2.0.0",
    "msw": "^0.29.0",
    "prettier": "^1.19.1",
    "rewire": "^4.0.1",
    "simple-git": "^2.36.1",
    "sinon": "^9.2.4",
    "ts-jest": "^26.5.3",
    "typescript": "^4.2.3",
    "vsce": "^1.88.0",
    "vscode-test": "^1.5.1"
  },
  "dependencies": {
    "temp": "^0.9.4",
    "cross-fetch": "^3.0.6",
    "dayjs": "^1.10.4",
    "graphql": "^15.5.0",
    "graphql-request": "^3.4.0",
    "https-proxy-agent": "^5.0.0",
    "request": "^2.88.0",
    "request-promise": "^4.2.6",
    "semver": "^7.3.5",
    "url": "^0.11.0"
  }
}
